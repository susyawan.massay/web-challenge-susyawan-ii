import "bootstrap/dist/css/bootstrap.css";
import styles from "../styles/Login.module.css";
import * as yup from "yup";
import { useFormik } from "formik";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";

export default function Login() {
  const router = useRouter();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (router.pathname !== "/login" && !isLoading) {
      router.push("/login");
    }
  }, [isLoading]);

  useEffect(() => {
    const token = sessionStorage.getItem("token");
    if (token) {
      router.push("/");
    }
  }, []);

  const validationLogin = yup.object().shape({
    userid: yup.string().required(),
    password: yup.string().required(),
  });

  const formik = useFormik({
    initialValues: {
      userid: "",
      password: "",
    },
    validationSchema: validationLogin,
    onSubmit: () => handleSubmit(),
  });

  async function handleSubmit() {
    const res = await fetch(`http://localhost:3000/api/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formik.values),
    }).then((data) => data.json());

    const stats = res.status;
    setIsLoading(stats);

    if (stats) {
      sessionStorage.setItem("token", stats);
      alert("Login Success");
      router.push("/");
    } else {
      alert("Wrong User ID or Password");
    }
  }

  function statusCheck() {
    !formik.values.userid || !formik.values.password
      ? alert("User ID or Password is Required")
      : formik.handleSubmit();
  }

  return (
    <>
      <div className="container-fluid px-0 mb-2">
        <div className="row row-cols-auto" style={{ maxHeight: "330px" }}>
          <div className="col-4 pe-0">
            <div style={{ maxHeight: "150px", maxWidth: "130px" }}>
              <img
                src="/assets/header-login.png"
                alt="login"
                className="w-100"
              />
            </div>
          </div>
          <div className="col-4 d-flex justify-content-center px-0">
            <div style={{ maxHeight: "150px", maxWidth: "150px" }}>
              <img src="/assets/logo.png" alt="logo" className="mt-5 w-100" />
            </div>
          </div>
          <div className="col-4 ps-0">
            <img
              src="/assets/header-login.png"
              alt="login"
              className="d-none"
            />
          </div>
        </div>
      </div>
      <div className="container-fluid mt-3">
        <div className="container">
          <div className="d-flex justify-content-center">
            <div className="d-flex flex-column" style={{ width: "300px" }}>
              <div className="d-flex flex-column my-3">
                <h4>Login</h4>
                <span className="text-muted" style={{ fontSize: "14px" }}>
                  Please sign in to continue.
                </span>
              </div>
              <div className="d-flex flex-column my-3">
                <form onSubmit={statusCheck}>
                  <span>User ID</span>
                  <div className="form-group">
                    <input
                      type="text"
                      className={`${styles.input} w-100`}
                      placeholder="User ID"
                      name="userid"
                      value={formik.values.userid}
                      onChange={formik.handleChange}
                    />
                  </div>
                  <span>Password</span>
                  <div className="form-group">
                    <input
                      type="password"
                      className={`${styles.input} w-100`}
                      placeholder="Password"
                      name="password"
                      value={formik.values.password}
                      onChange={formik.handleChange}
                    />
                  </div>
                </form>
                <div className="d-flex justify-content-end my-2">
                  <button
                    onClick={statusCheck}
                    className="btn btn-light px-5 text-white"
                    style={{ borderRadius: "50px", backgroundColor: "purple" }}
                  >
                    LOGIN
                  </button>
                </div>
                <div></div>
              </div>
              <div className="d-flex my-5 justify-content-center">
                <span style={{ fontSize: "12px" }}>
                  Don't have an account?{" "}
                  <a
                    href="/"
                    className="text-decoration-none"
                    style={{ color: "tomato" }}
                  >
                    Sign Up
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
